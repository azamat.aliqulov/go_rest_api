package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	quizController
	db
)
func helloWorld(w http.ResponseWriter, r *http.Request)  {
	fmt.Printf("Hello world")
}

func handleRequest()  {
	myRouter:=mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", helloWorld).Methods("GET")
	myRouter.HandleFunc("/users", AllUsers).Methods("GEt")
	fmt.Println("Server at 8082")
	log.Fatal(http.ListenAndServe(":8082", myRouter))
}

func main()  {
	fmt.Println("Go ORM Tutorial")

	// db_connection()
	handleRequest()
}